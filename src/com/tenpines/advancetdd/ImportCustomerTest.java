package com.tenpines.advancetdd;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Restrictions;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ImportCustomerTest {

    private Session session;

    public void importCustomers() throws IOException {

        FileReader reader = new FileReader("resources/input.txt");
        LineNumberReader lineReader = new LineNumberReader(reader);

        Customer newCustomer = null;
        String line = lineReader.readLine();
        while (line!=null) {
            if (line.startsWith("C")){
                String[] customerData = line.split(",");
                newCustomer = new Customer();
                newCustomer.setFirstName(customerData[1]);
                newCustomer.setLastName(customerData[2]);
                newCustomer.setIdentificationType(customerData[3]);
                newCustomer.setIdentificationNumber(customerData[4]);
                session.persist(newCustomer);
            }
            else if (line.startsWith("A")) {
                String[] addressData = line.split(",");
                Address newAddress = new Address();

                newCustomer.addAddress(newAddress);
                newAddress.setStreetName(addressData[1]);
                newAddress.setStreetNumber(Integer.parseInt(addressData[2]));
                newAddress.setTown(addressData[3]);
                newAddress.setZipCode(Integer.parseInt(addressData[4]));
                newAddress.setProvince(addressData[5]);
            }

            line = lineReader.readLine();
        }

        reader.close();
    }

    @After
    public void tearDown() {
        session.getTransaction().commit();
        session.close();
    }

    @Before
    public void setUp() {
        Configuration configuration = new Configuration();
        configuration.configure();

        ServiceRegistry serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties()).buildServiceRegistry();
        SessionFactory sessionFactory = configuration.buildSessionFactory(serviceRegistry);
        session = sessionFactory.openSession();
        session.beginTransaction();
    }

    @Test
    public void test1() throws IOException {
        importCustomers();

        List customers = session.createCriteria(Customer.class).list();

        assertEquals(2, customers.size());

        customers = session.createCriteria(Customer.class)
                .add(Restrictions.eq("identificationType", "D"))
                .add(Restrictions.eq("identificationNumber", "22333444"))
                .add(Restrictions.eq("firstName", "Pepe"))
                .add(Restrictions.eq("lastName", "Sanchez"))
                .list();

        assertEquals(1, customers.size());
        Customer pepeSanchez = (Customer) customers.get(0);
        assertEquals("Pepe",pepeSanchez.getFirstName());
        assertEquals("Sanchez",pepeSanchez.getLastName());
        assertEquals("D",pepeSanchez.getIdentificationType());
        assertEquals("22333444",pepeSanchez.getIdentificationNumber());

        customers = session.createCriteria(Customer.class)
                .add(Restrictions.eq("identificationType", "C"))
                .add(Restrictions.eq("identificationNumber", "23-25666777-9"))
                .add(Restrictions.eq("firstName", "Juan"))
                .add(Restrictions.eq("lastName", "Perez"))
                .list();
        assertEquals(1, customers.size());
        Customer juanPerez = (Customer) customers.get(0);
        assertEquals("Juan",juanPerez.getFirstName());
        assertEquals("Perez",juanPerez.getLastName());
        assertEquals("C",juanPerez.getIdentificationType());
        assertEquals("23-25666777-9",juanPerez.getIdentificationNumber());
    }

    @Test
    public void test2() throws IOException {
        importCustomers();

        List<Customer> customers = session.createCriteria(Customer.class)
                .add(Restrictions.eq("identificationType", "D"))
                .add(Restrictions.eq("identificationNumber", "22333444"))
                .list();
        Customer pepeSanchez = customers.get(0);

        Address address = pepeSanchez.getAddressAt("San Martin");
        assertEquals(3322, address.getStreetNumber());
        assertEquals(1636, address.getZipCode());
        assertEquals("BsAs", address.getProvince());
        assertEquals("Olivos", address.getTown());

        address = pepeSanchez.getAddressAt("Maipu");
        assertEquals(888, address.getStreetNumber());
        assertEquals(1122, address.getZipCode());
        assertEquals("Buenos Aires", address.getProvince());
        assertEquals("Florida", address.getTown());
    }
}
